# Undelete files from btrfs filesystem

In short, I mistakenly overwrote my important save file, hence this article for recording my progress.

## Configuration

### File system path to the file to be undeleted

/@home/brlin/下載/some-game/game/saves/1-1-1.save

According to the surrounding save's timestamp, it is created between 2023/2/26 02:34 to 03:07.

### Source file system residing block device

File system where the file to be undeleted resides:

/dev/sda1

### Source system

Ubuntu 22.04.1 LTS

## Process

### Trying to restore overwritten file using undelete-btrfs

1. Unmount the filesystem, if the filesystem cannot unmount in the current boot configuration (like it is mounted in the /home directory), shutdown the current system and boot into a live OS installer session(it should ship a version of btrfs-progs more recent the on in the original system to avoid conpatiblity issues) instead.
1. Clone this Git repository to local.
1. Update the undelete-btrfs submodule to the latest revision.
1. (Optional) Mount the filesystem to store recovered file at the [recovered-files](recovered-files) subfolder:

    ```sh
    sudo mount _filesystem_medium_ /path/to/recovered-files
    ```

1. Run the following command to launch the undelete utility:

    ```sh
    sudo ./undelete-btrfs/undelete.sh _source_filesystem_ recovered-files
    ```

1. At the `Enter the path to a file or directory` prompt, enter [file system path to the file to be undeleted](#File-to-be-undeleted).
1. The recover process should start and (if lucky) will end like this:

    ```output
    ========================================================
    | Undelete-BTRFS | Recovery completed | Depth-level: 0 |
    ========================================================
    Recovery completed at depth level 0! 
     ==> 3 non-empty files found in recovered-files/.

    Here's a small sample of 'find recovered-files/ -type f' output:
    ========
    recovered-files/@home/brlin/下載/some-game/game/saves/1-1-1.save
    recovered-files/.gitignore
    recovered-files/README.md
    ========
    (Showing max 20 files)

    Are you happy with the results?
    1) Yes, exit script. 
    2) No, try a deeper level restore. 
    3) No, I want to try a different path.
    ```

1. Check the recovered file, hopefully it is the one you've accidentally overwritten..., if not, try deeper level restore, I guess...

### Trying to restore overwritten file using PhotoRec

Unfortunately I was unable to recover my save file using undelete-btrfs...the recovered file from depth 1 and 2 is both the overwritten version and the depth 3 version is not the expected file format thus is discarded as well.

We use the old fashion way by sniffing all the files matching the same type of the game save and locate the matching file.  Ren'Py's game saves are essentially ZIP archives so it can be matched using the same file magic IDs however by default PhotoRec's ZIP archives has no matching size limit so an oversized file may fill the recovered-file storage easily so it must be patched to match game saves which is typically smaller than 500KiB.

1. Add deb-src source repositories to APT's source list:

    ```apt-source-list
    deb-src http://tw.archive.ubuntu.com/ubuntu/ jammy main restricted universe multiverse
    deb-src http://tw.archive.ubuntu.com/ubuntu/ jammy-updates main restricted universe multiverse
    ```

1. Run the following command in a text terminal to update APT's software source cache:

    ```sh
    sudo apt update
    ```

1. Run the following command to install TestDisk's build dependency packages:

    ```sh
    sudo apt build-dep testdisk
    ```

1. Download TestDisk source archive(TestDisk software contains the PhotoRec utility) from [the download page](https://www.cgsecurity.org/wiki/TestDisk_Download), as the stable release version can't easily buildable in Ubuntu 22.04 I use the Beta version instead.
1. Extract source archive
1. Open a text terminal application
1. Change working directory to the extracted source archive folder
1. Run the following command to patch PhotoRec to limit the max filesize of matching ZIP archives to 350KiB:

    ```sh
    patch \
        --strip=1 \
        < /path/to/patches/set-stricter-zip-file-max-size.patch
    ```

1. Run the following command to configure the build:

    ```sh
    ./configure \
        --prefix="${PWD}/prefix"
    ```

1. Run the following command to build TestDisk:

    ```sh
    make \
        --jobs="$(nproc)"
    ```

1. Run the following command to launch PhotoRec application:

    ```sh
    sudo ./prefix/bin/photorec
    ```

1. Select disk and partition of the source filesystem
1. In the partition selection screen, select the "File Opt" option to edit the filetypes to discover, only choose the zip filetype
1. Select "Search" continue
1. Select "Other" filesystem type
1. Select the folder to store recovered file in and press C to start the recovery process.
1. From the recovered files, find the file that matches the overwritten file, I wrote [a script to accelerate this process for my own case](find-file-with-matching-timestamp.sh).

## Conslusion

In the end I wasn't able to recover the overwritten file as it seems to be literally overwritten:

```output
Info: The following recovered files containing matching timestamp:

* ./recovered-files/recup_dir.1/f13868696_screenshot.zip(23-Feb-26 03:09)
* ./recovered-files/recup_dir.1/f9191128_screenshot.zip(23-Feb-26 03:07)
* ./recovered-files/recup_dir.1/f9626528_screenshot.zip(23-Feb-26 02:34)
* ./recovered-files/recup_dir.1/f9655544_screenshot.zip(23-Feb-26 03:09)
* ./recovered-files/recup_dir.1/f9725416_screenshot.zip(23-Feb-26 03:07)
* ./recovered-files/recup_dir.1/f9857432_screenshot.zip(23-Feb-26 02:34)
Info: Operation completed without errors.
```

Oh well.

## References

* [GitHub - danthem/undelete-btrfs: A script that attempts to undelete files in a BTRFS file system. Make sure you read the README.](https://github.com/danthem/undelete-btrfs)  
  The god savor...maybe
* [How to exclude some files to be recovered? - cgsecurity.org](https://forum.cgsecurity.org/phpBB3/viewtopic.php?t=526)  
  Details on how to limit the file size of a file type magic to match.

