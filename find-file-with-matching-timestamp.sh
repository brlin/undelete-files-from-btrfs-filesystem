#!/usr/bin/env bash
# Find file with the matching modified time timestamp
set -eu
shopt -s nullglob

script="${0}"
script_dir="${script%/*}"
recovered_files_basedir="${script_dir}/recovered-files"

recovered_files=("${recovered_files_basedir}/"recup_dir.*/*.zip)

matching_timestamp_regexes=(
    #'23-Feb-26 02:[[:digit:]]{2}'
    '23-Feb-26 02:3[[:digit:]]'
    '23-Feb-26 02:4[[:digit:]]'
    '23-Feb-26 02:5[[:digit:]]'
    #'23-Feb-26 03:[[:digit:]]{2}'
    '23-Feb-26 03:0[[:digit:]]'
)

grep_opts=(
    --extended-regexp
    --only-matching
    --max-count=1
)

matched_files=()
matched_timestamps=()

for recovered_file in "${recovered_files[@]}"; do
    printf -- \
        'Info: Checking "%s" recovered file...\n' "${recovered_file}"
    if ! zipinfo_output="$(
        zipinfo \
            "${recovered_file}"
        )"; then
        printf -- \
            "Warning: Unable to parse recovered file(%s)'s content, skipping...\\n" \
            "${recovered_file}" \
            1>&2
        continue
    fi
    
    for matching_timestamp_regex in "${matching_timestamp_regexes[@]}"; do
        if matched_timestamp="$(
            grep \
                "${grep_opts[@]}" \
                "${matching_timestamp_regex}" \
                <<<"${zipinfo_output}"
            )"; then
            printf -- \
                'Info: Timestamp matched for recovered file "%s".\n' \
                "${recovered_file}"
            matched_files+=("${recovered_file}")
            matched_timestamps+=("${matched_timestamp}")
        fi
    done
done

printf -- \
    'Info: The following recovered files containing matching timestamp:\n\n'
i=0
for matched_file in "${matched_files[@]}"; do
    printf -- \
        '* %s(%s)\n' \
        "${matched_file}" \
        "${matched_timestamps["${i}"]}"
    ((i = i + 1))
done

printf -- \
    'Info: Operation completed without errors.\n'

