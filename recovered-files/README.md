# recovered-files

This is where the recovered files should be saved, note that `git clean -dfx` shouldn't be run so that the files won't be erased.

